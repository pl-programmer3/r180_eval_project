import fdb
import xlsxwriter
from decimal import *

###########################################################################
######################  DATABASE CONNECTION   #### ########################
###########################################################################

con = fdb.connect(dsn='10.0.0.80/3050:D:\Quantum\Database\Practice.FDB', user='Programmer1', password='F*s@5x9CrK')

cur = con.cursor()

##########################################################################
#######################      QUERIES    ##################################
##########################################################################

SELECT_ALL = 'select db_name()'
SALES_ORDER_MAIN_PART_NUMBER_5_YEAR = """select sd.sod_auto_key, pm.pn_stripped, pcc.CONDITION_CODE, sh.so_number, sd.unit_price, sd.qty_invoiced, (sd.qty_invoiced * sd.unit_price) as SO_TOTAL from parts_master pm, so_detail sd, so_header sh, part_condition_codes pcc
where sd.pnm_auto_key = pm.pnm_auto_key and sd.soh_auto_key = sh.soh_auto_key and sd.PCC_AUTO_KEY = pcc.PCC_AUTO_KEY and sd.entry_date > (current_timestamp - (5*365)) and sd.unit_price > .01"""

SALES_ORDER_ALTERNATE_PART_NUMBER_5_YEAR = """select s.sod_auto_key, m.pn_stripped, pcc.CONDITION_CODE, sh.so_number, s.unit_price, s.qty_invoiced, (s.unit_price * s.qty_invoiced) as SO_TOTAL from ALTERNATES_PARTS_MASTER a join so_detail s on s.pnm_auto_key = a.alt_pnm_auto_key join so_header sh on s.soh_auto_key = sh.soh_auto_key join part_condition_codes pcc on s.PCC_AUTO_KEY = pcc.PCC_AUTO_KEY join parts_master m on m.pnm_auto_key = a.pnm_auto_key where s.entry_date > (current_timestamp - (5*365)) and s.unit_price > .01"""

STOCK =	 """select s.STM_AUTO_KEY, pm.pn_stripped, s.qty_oh as Total_Qty_OH from parts_master pm,stock s where pm.pnm_auto_key = s.pnm_auto_key and s.qty_oh > 0"""

STOCK_ALTERNATE_PART_NUMBER = """select s.STM_AUTO_KEY, m.pn_stripped, s.qty_oh as Total_Qty_OH from ALTERNATES_PARTS_MASTER a join stock s on s.pnm_auto_key = a.alt_pnm_auto_key join parts_master m on m.pnm_auto_key = a.pnm_auto_key where s.qty_oh > 0"""

VENDOR_QUOTE = """select vq.VQD_AUTO_KEY, pm.pn_stripped, vq.VQ_NUMBER, vq.unit_cost, vq.QTY_QUOTED, (vq.unit_cost * vq.QTY_QUOTED) as VQ_TOTAL, pcc.CONDITION_CODE from parts_master pm, vq_detail vq join part_condition_codes pcc on vq.PCC_AUTO_KEY = pcc.PCC_AUTO_KEY where vq.pnm_auto_key = pm.pnm_auto_key and vq.unit_cost > .01"""

VENDOR_QUOTE_ALTERNATE_PART_NUMBERS = """select v.VQD_AUTO_KEY, m.pn_stripped, v.VQ_NUMBER, v.unit_cost, v.QTY_QUOTED, (v.unit_cost * v.QTY_QUOTED) as VQ_TOTAL, pcc.CONDITION_CODE from ALTERNATES_PARTS_MASTER a join vq_detail v on v.pnm_auto_key = a.alt_pnm_auto_key join part_condition_codes pcc on v.PCC_AUTO_KEY = pcc.PCC_AUTO_KEY join parts_master m on m.pnm_auto_key = a.pnm_auto_key where v.unit_cost > .01"""

CUSTOMER_QUOTE = """select cd.CQD_AUTO_KEY, pm.pn_stripped, ch.CQ_NUMBER, cd.unit_price, cd.QTY_QUOTED, (cd.unit_price * cd.QTY_QUOTED) as CQ_TOTAL, pcc.CONDITION_CODE from parts_master pm, CQ_detail cd, CQ_HEADER ch join part_condition_codes pcc on cd.PCC_AUTO_KEY = pcc.PCC_AUTO_KEY where cd.pnm_auto_key = pm.pnm_auto_key and cd.CQH_AUTO_KEY = ch.CQH_AUTO_KEY and cd.unit_price > .01"""

CUSTOMER_QUOTE_ALTERNATE_PART_NUMBERS = """select q.cqd_auto_key, m.pn_stripped, ch.cqh_auto_key, q.unit_price, q.QTY_QUOTED, (q.unit_price * q.QTY_QUOTED) CQ_TOTAL, pcc.CONDITION_CODE from ALTERNATES_PARTS_MASTER a join cq_detail q on a.alt_pnm_auto_key = q.pnm_auto_key join part_condition_codes pcc on q.PCC_AUTO_KEY = pcc.PCC_AUTO_KEY join cq_header ch on q.CQH_AUTO_KEY = ch.CQH_AUTO_KEY join parts_master m on a.pnm_auto_key = m.pnm_auto_key where q.unit_price > .01"""

###############################################################################
#######################   EXECUTE QUERIES   ###################################
###############################################################################
'''
#<result set row with STM_AUTO_KEY = 13, TOTAL_QTY_OH = 11, PN_STRIPPED = 23009630>
#(13, '23009630', Decimal('11'))
cur.execute(STOCK)
pnArray = dict()
for row in cur.fetchall():
        if row[2] != None:
                if not pnArray:
                        pnArray[row[1]] = row[2]
                if row[1] in pnArray:
                        pnArray[row[1]] += row[2]
                else:
                        pnArray[row[1]] = row[2]
for key in pnArray:
        print 'Part Number:%-20s Total Qty OH:%s' % (key, pnArray[key])
'''
#<result set row with SO_NUMBER = S114280, QTY_INVOICED = 2, UNIT_PRICE = 2600.0, PN_STRIPPED = 205001915001, SOD_AUTO_KEY = 92415, SO_TOTAL = 5200.0, CONDITION_CODE = NS>
#(92415, '205001915001', 'NS', 'S114280', 2600.0, Decimal('2'), 5200.0)
"SOpnArray[Part Number]:[Qty Sold, Qty Quoted, Unit Price, SO_Count, CQ_Count, VQ_Count] "
SO_Count = 0 
cur.execute(SALES_ORDER_MAIN_PART_NUMBER_5_YEAR)
SOpnArray = dict()
for row in cur.fetchall():
	SO_Count = SO_Count + 1
        if row[5] != None:
                if not SOpnArray:
                        SOpnArray[row[1]] = [row[5], 0, row[4], 1, 0, 0]
                if row[1] in SOpnArray:
                        SOpnArray[row[1]][0] += row[5]
			SOpnArray[row[1]][3] += 1
                else:
                        SOpnArray[row[1]] = [row[5], 0, row[4], 1, 0, 0]

#<result set row with SO_NUMBER = S114280, QTY_INVOICED = 2, UNIT_PRICE = 2600.0, PN_STRIPPED = 2050019151, SOD_AUTO_KEY = 92415, SO_TOTAL = 5200.0, CONDITION_CODE = NS>
#(92415, '2050019151', 'NS', 'S114280', 2600.0, Decimal('2'), 5200.0)
cur.execute(SALES_ORDER_ALTERNATE_PART_NUMBER_5_YEAR) 
altSOpnArray = dict()
for row in cur.fetchall():
	SO_Count = SO_Count + 1
        if row[5] != None:
                if not altSOpnArray:
                        altSOpnArray[row[1]] = [row[5], 0, row[4], 1, 0, 0]
                if row[1] in altSOpnArray:
                        altSOpnArray[row[1]][0] += row[5]
			altSOpnArray[row[1]][3] += 1
                else:
                        altSOpnArray[row[1]] = [row[5], 0, row[4], 1, 0, 0]

'''
#<result set row with STM_AUTO_KEY = 25, TOTAL_QTY_OH = 1, PN_STRIPPED = 206040410005>
#(25, '206040410005', Decimal('1'))
cur.execute(STOCK_ALTERNATE_PART_NUMBER)
pnArray = dict()
for row in cur.fetchall():
	if row[2] != None:
                if not pnArray:
                        pnArray[row[1]] = row[2]
                if row[1] in pnArray:
                        pnArray[row[1]] += row[2]
                else:
                        pnArray[row[1]] = row[2]

for key in pnArray:
        print 'Part Number:%-20s Total Qty OH:%s' % (key, pnArray[key])
'''
#<result set row with VQD_AUTO_KEY = 32, VQ_NUMBER = Q100645, PN_STRIPPED = 41800044, QTY_QUOTED = 1, UNIT_COST = 850.0, VQ_TOTAL = 850.0, CONDITION_CODE = NE>
#(32, '41800044', 'Q100645', 850.0, Decimal('1'), 850.0, 'NE')
VQ_Count = 0
cur.execute(VENDOR_QUOTE)
VQpnArray = dict()
for row in cur.fetchall():
	VQ_Count = VQ_Count + 1
        if row[4] != None:
                if row[1] in SOpnArray:
                        SOpnArray[row[1]][1] += row[4]
			SOpnArray[row[1]][5] += 1


#<result set row with VQD_AUTO_KEY = 32, VQ_NUMBER = Q100645, PN_STRIPPED = TRU66A, QTY_QUOTED = 1, UNIT_COST = 850.0, VQ_TOTAL = 850.0, CONDITION_CODE = NE>
#(32, 'TRU66A', 'Q100645', 850.0, Decimal('1'), 850.0, 'NE')
cur.execute(VENDOR_QUOTE_ALTERNATE_PART_NUMBERS)
altVQpnArray = dict()
for row in cur.fetchall():
	VQ_Count = VQ_Count + 1
        if row[4] != None:
                if row[1] in altSOpnArray:
                        altSOpnArray[row[1]][1] += row[4]
			altSOpnArray[row[1]][5] += 1

#<result set row with CQ_TOTAL = 40.3, CQD_AUTO_KEY = 3, UNIT_PRICE = 3.1, PN_STRIPPED = 4042T88P01, QTY_QUOTED = 13, CQ_NUMBER = Q100604, CONDITION_CODE = NS>
#(3, '4042T88P01', 'Q100604', 3.1, Decimal('13'), 40.300000000000004, 'NS')
CQ_COUNT = 0
cur.execute(CUSTOMER_QUOTE)
CQpnArray = dict()
for row in cur.fetchall():
        CQ_COUNT + CQ_COUNT + 1
	if row[4] != None:
                if row[1] in SOpnArray:
                        SOpnArray[row[1]][1] += row[4]
			SOpnArray[row[1]][4] += 1


#<result set row with CQH_AUTO_KEY = 4, CQ_TOTAL = 1460.0, CQD_AUTO_KEY = 5, UNIT_PRICE = 730.0, PN_STRIPPED = 0106937, QTY_QUOTED = 2, CONDITION_CODE = NS>
#(5, '0106937', 4, 730.0, Decimal('2'), 1460.0, 'NS')
cur.execute(CUSTOMER_QUOTE_ALTERNATE_PART_NUMBERS)
altCQpnArray = dict()
for row in cur.fetchall():
	CQ_COUNT = CQ_COUNT + 1
	if row[4] != None:
		if row[1] in altSOpnArray:
			altSOpnArray[row[1]][1] += row[4]
			altSOpnArray[row[1]][4] += 1

######################      CREATE AND WRITE TO EXCEL FILE        #################################################

workbook = xlsxwriter.Workbook('eval.xlsx')
worksheet = workbook.add_worksheet()

##    ADDING HEADERS    ##
worksheet.write('A1', 'Part Number')
worksheet.write('B1', 'Qty Sold')
worksheet.write('C1', 'Qty Quoted')
worksheet.write('D1', 'Unit Price')
worksheet.write('E1', 'SO Count')
worksheet.write('F1', 'CQ Count')
worksheet.write('G1', 'VQ Count')

worksheet.write('I1', 'Alt Part Number')
worksheet.write('J1', 'Qty Sold')
worksheet.write('K1', 'Qty Quoted')
worksheet.write('L1', 'Unit Price')
worksheet.write('M1', 'SO Count')
worksheet.write('N1', 'CQ Count')
worksheet.write('O1', 'VQ Count')

rowNum = 1
##    WRITING DATA TO THE DOCUMENT   ##
"SOpnArray[Part Number]:[Qty Sold, Qty Quoted, Unit Price, SO_Count, CQ_Count, VQ_Count] "
for key in SOpnArray:
	if SOpnArray[key][4] !=0 and (SOpnArray[key][3] / SOpnArray[key][4]) < 2.32  and SOpnArray[key][0] > 0:
		worksheet.write(rowNum, 0, key)
		worksheet.write(rowNum, 1, SOpnArray[key][0])
		worksheet.write(rowNum, 2, SOpnArray[key][1])
		worksheet.write(rowNum, 3, SOpnArray[key][2])
                worksheet.write(rowNum, 4, SOpnArray[key][3])
                worksheet.write(rowNum, 5, SOpnArray[key][4])
                worksheet.write(rowNum, 6, SOpnArray[key][5])
		rowNum += 1
#		print 'PN: %-20s QTY SOLD: %-5s QTY QUOTED: %-5s' % (key, SOpnArray[key][0], SOpnArray[key][1])

rowNum = 1
for key in altSOpnArray:
	if altSOpnArray[key][4] !=0 and (altSOpnArray[key][3] / altSOpnArray[key][4]) < 2.32  and altSOpnArray[key][0] > 0:
                worksheet.write(rowNum, 8, key)
                worksheet.write(rowNum, 9, altSOpnArray[key][0])
                worksheet.write(rowNum, 10, altSOpnArray[key][1])
                worksheet.write(rowNum, 11, altSOpnArray[key][2])
                worksheet.write(rowNum, 12, altSOpnArray[key][3])
                worksheet.write(rowNum, 13, altSOpnArray[key][4])
                worksheet.write(rowNum, 14, altSOpnArray[key][5])
                rowNum += 1

workbook.close()
'''
for key in altSOpnArray:
	if (altSOpnArray[key][0] * 2) < altSOpnArray[key][1]:
		print 'ALT PN: %-20s QTY SOLD: %-5s QTY QUOTED: %-5s' % (key, altSOpnArray[key][0], altSOpnArray[key][1])
'''
#print "SO_COUNT: %s			VQ_COUNT: %s			CQ_COUNT: %s" % (SO_Count, VQ_Count, CQ_COUNT)



